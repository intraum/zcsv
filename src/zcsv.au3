#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\MyAutoIt3_Blue.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=Замены в CSV
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Script Function:
	Замены в CSV запятых на точки с запятыми, либо наоборот

#ce ----------------------------------------------------------------------------

#Region ; зависимости

#include <Array.au3>
#include <File.au3>
#include "zavisimosti\FileAux.au3"
#include "zavisimosti\GUIAux.au3"
;для GUI
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <GuiButton.au3>

#EndRegion ; зависимости


#Region ; настройки AutoIt

Opt("GUIOnEventMode", 1)

#EndRegion ; настройки AutoIt


#Region ; обычные глобальные переменные

Global $esMsgBoxTitle = "Замены в CSV"

#EndRegion ; обычные глобальные переменные


#Region ; класс сообщений

Global Const $esMessageGUID = "241c12e9-fda3-48dc-a3f9-95eff928648e"

Global Enum _
		$eiMess_iGUID, _        ; GUID
		$eiMess_sMessage, _     ; сообщение для юзера
		$eiMess_iFlag, _        ; флаг для окна сообщения
		$eiMess_iSize           ; размер массива-"объекта"

; конструктор

Func Message()

	Local $hMessage[$eiMess_iSize]

	$hMessage[$eiMess_iGUID] = $esMessageGUID

	Return $hMessage

EndFunc   ;==>Message


; частные методы _IsObject для проверок, тот ли "объект"

Func _Message_IsObject(ByRef Const $hMessage)

	Return UBound($hMessage) = $eiMess_iSize And _
			$hMessage[$eiMess_iGUID] == $esMessageGUID

EndFunc   ;==>_Message_IsObject


Func Message_Set(ByRef $hMessage, $sMessage, $iFlag)

	If Not _Message_IsObject($hMessage) Then Return

	$hMessage[$eiMess_sMessage] = $sMessage

	$hMessage[$eiMess_iFlag] = $iFlag

EndFunc   ;==>Message_Set


Func Message_ShowSavedExit(ByRef Const $hMessage)

	If Not _Message_IsObject($hMessage) Then Return

	_MsgBoxFireExit($hMessage[$eiMess_iFlag], $esMsgBoxTitle, $hMessage[$eiMess_sMessage])

EndFunc   ;==>Message_ShowSavedExit

#EndRegion ; класс сообщений


#Region ; класс графического интерфейса

Global Const _
		$esGUI_Radio1 = '[CLASS:Button; INSTANCE:1]', _  ; идентификатор первого радиобатона
		$esGUI_Chkbox = '[CLASS:Button; INSTANCE:4]'     ; идентификатор чекбокса

Global Enum _
		$eiGUI_sSearch, _   ; строка поиска
		$eiGUI_sReplace, _  ; строка замены
		$eiGUI_iSize        ; размер массива-"объекта"

; конструктор интерфейса

Func GUI()

	Local $hWnd

	$hWnd = GUICreate($esMsgBoxTitle, 282, 234, -1, -1, -1, -1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "GUI_Exit", $hWnd)
	GUICtrlCreateRadio("точка с запятой ==> запятая", 40, 54, 209, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Во всех CSV файлах (в папке со скриптом) символ ; будет заменён на символ ,")
	GUICtrlCreateRadio("запятая ==> точка с запятой", 40, 86, 209, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Во всех CSV файлах (в папке со скриптом) символ , будет заменён на символ ;")
	GUICtrlCreateLabel("Тип замены", 40, 25, 102, 15, -1, -1)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	GUICtrlCreateButton("Старт", 100, 167, 100, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "ExecOperations")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateCheckbox("учитывать кавычки", 40, 120, 150, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Поиск и замена будут произведены с оборачиванием запятой или точки запятой в кавычки")

	Return $hWnd

EndFunc   ;==>GUI


; вызов конструктора интерфейса с его инициализацией

Func GUI_Init()

	Local $hWnd = GUI()

	GUISetState(@SW_SHOW, $hWnd)

EndFunc   ;==>GUI_Init


; конструктор объекта с данными об интерфейсе

Func GUI_Data(ByRef Const $hWnd)

	Local $hGUI[$eiGUI_iSize]

	Local $sJacket = _GUICtrlChkboxRadRead(_GUIGetCtrlID($hWnd, $esGUI_Chkbox)) ? '"' : ''

	If _GUICtrlChkboxRadRead(_GUIGetCtrlID($hWnd, $esGUI_Radio1)) Then

		$hGUI[$eiGUI_sSearch] = $sJacket & ";" & $sJacket
		$hGUI[$eiGUI_sReplace] = $sJacket & "," & $sJacket
	Else
		$hGUI[$eiGUI_sSearch] = $sJacket & "," & $sJacket
		$hGUI[$eiGUI_sReplace] = $sJacket & ";" & $sJacket

	EndIf

	Return $hGUI

EndFunc   ;==>GUI_Data


Func GUI_Exit()

	Exit

EndFunc   ;==>GUI_Exit

#EndRegion ; класс графического интерфейса


#Region ; класс CSV

Global Const $esCSVGUID = "bd55e507-08bc-487d-b898-91e35200d0d3"

Global Enum _
		$eiCSV_iGUID, _           ; GUID
		$eiCSV_aFilesPaths, _     ; массив с путями файлов для обработки
		$eiCSV_sFilePath, _       ; путь к текущему файлу
		$eiCSV_hFile, _           ; дескриптор текущего файла
		$eiCSV_sFileContents, _   ; содержимое текущего файла
		$eiCSV_bSkipped, _        ; был ли пропущен хотя бы 1 файл
		$eiCSV_bModified, _       ; был ли обработан хотя бы 1 файл
		$eiCSV_iSize              ; размер массива-"объекта"

; конструктор

Func CSV()

	Local $hCSV[$eiCSV_iSize]

	$hCSV[$eiCSV_iGUID] = $esCSVGUID

	$hCSV[$eiCSV_bModified] = False
	$hCSV[$eiCSV_bSkipped] = False

	Return $hCSV

EndFunc   ;==>CSV


Func _CSV_IsObject(ByRef Const $hCSV)

	Return UBound($hCSV) = $eiCSV_iSize And _
			$hCSV[$eiCSV_iGUID] == $esCSVGUID

EndFunc   ;==>_CSV_IsObject


; установка логических значений без переназначения

Func _CSV_SetStatus(ByRef $hCSV, $i)

	If Not $hCSV[$i] Then $hCSV[$i] = True

EndFunc   ;==>_CSV_SetStatus


Func CSV_SetFiles(ByRef $hCSV, ByRef $hMessage)

	If Not _CSV_IsObject($hCSV) Then Return

	If Not _DirWritable(@ScriptDir) Then

		Message_Set($hMessage, _
				"Папка недоступна для записи (возможно, не хватает прав администратора)", $MB_ICONWARNING)

		Return SetError(1)
	EndIf

	$hCSV[$eiCSV_aFilesPaths] = _FileListToArray(@ScriptDir, "*.csv", $FLTA_FILES, True)

	If Not @error Then

		Return

	ElseIf @error = 4 Then

		Message_Set($hMessage, "В текущей папке не найден ни один CSV файл", $MB_ICONINFORMATION)

	ElseIf @error Then

		Message_Set($hMessage, "Ошибка при подборе CSV файлов", $MB_ICONERROR)

	EndIf

	SetError(2)

EndFunc   ;==>CSV_SetFiles


Func CSV_ProcessFiles(ByRef $hCSV, ByRef Const $hGUI)

	If Not _CSV_IsObject($hCSV) Then Return

	For $i = 1 To ($hCSV[$eiCSV_aFilesPaths])[0]

		$hCSV[$eiCSV_sFilePath] = ($hCSV[$eiCSV_aFilesPaths])[$i]

		If _CSV_SkipFile($hCSV) Then ContinueLoop

		_CSV_FileOpen($hCSV)

		If @error Then ContinueLoop

		_CSV_FileRead($hCSV)

		_CSV_FileClose($hCSV)

		If Not _CSV_FileSuits($hCSV, $hGUI) Then ContinueLoop

		_CSV_FileOpen($hCSV, False)

		If @error Then ContinueLoop

		_CSV_FileChangeContents($hCSV, $hGUI)

		_CSV_FileWrite($hCSV)

		_CSV_FileClose($hCSV)
	Next
EndFunc   ;==>CSV_ProcessFiles


Func CSV_CheckFlags(ByRef Const $hCSV, ByRef $hMessage)

	If Not _CSV_IsObject($hCSV) Then Return

	If $hCSV[$eiCSV_bModified] And Not $hCSV[$eiCSV_bSkipped] Then

		Return ; по крайней мере 1 файл был изменён и ни один не был пропущен
	Else
		If $hCSV[$eiCSV_bSkipped] Then ; по крайней мере 1 файл был пропущен

			Local $sFrag = @CRLF _
					 & "Но как минимум 1 файл был пропущен либо из-за того," _
					 & " что занят другим процессом, либо из-за атрибутов" _
					 & " (только для чтения/системный/скрытый)"

			If $hCSV[$eiCSV_bModified] Then ; по крайней мере 1 файл был изменён

				Message_Set($hMessage, "Работа завершена." & $sFrag, $MB_ICONWARNING)
			Else
				Message_Set($hMessage, "Ни один файл не был изменён." & $sFrag, $MB_ICONWARNING)
			EndIf

		Else ; ни один файл не был изменён и ни один не был пропущен

			Message_Set($hMessage, "Замен не требуется (ни один файл не был изменён)", $MB_ICONINFORMATION)
		EndIf
	EndIf

	SetError(1)

EndFunc   ;==>CSV_CheckFlags


Func _CSV_SkipFile(ByRef $hCSV)

	; условия пропуска:
	;    * файл имеет один из атрибутов: только чтение, скрытый, системный;
	;    * занят другим процессом;

	If _FileInUse($hCSV[$eiCSV_sFilePath]) Then

		_CSV_SetStatus($hCSV, $eiCSV_bSkipped)

		Return True

	EndIf

	Local $sAttribs = FileGetAttrib($hCSV[$eiCSV_sFilePath])

	If _
			StringInStr($sAttribs, "R") Or _
			StringInStr($sAttribs, "H") Or _
			StringInStr($sAttribs, "S") Then

		_CSV_SetStatus($hCSV, $eiCSV_bSkipped)

		Return True
	EndIf

	Return False

EndFunc   ;==>_CSV_SkipFile


Func _CSV_FileOpen(ByRef $hCSV, $bReadOnly = True)

	Local $iFlag = $bReadOnly ? $FO_READ : $FO_OVERWRITE

	Switch FileGetEncoding($hCSV[$eiCSV_sFilePath]) ; открываем файл с сохранением кодировки

		Case "256"

			$iFlag += $FO_UTF8_NOBOM

		Case "512"

			$iFlag += $FO_ANSI

		Case "128"

			$iFlag += $FO_UTF8

	EndSwitch

	$hCSV[$eiCSV_hFile] = FileOpen($hCSV[$eiCSV_sFilePath], $iFlag)

	If Not ($hCSV[$eiCSV_hFile] = -1) Then Return

	_CSV_SetStatus($hCSV, $eiCSV_bSkipped)

	SetError(1)

EndFunc   ;==>_CSV_FileOpen


Func _CSV_FileRead(ByRef $hCSV)

	$hCSV[$eiCSV_sFileContents] = FileRead($hCSV[$eiCSV_sFilePath])

	If @error Then $hCSV[$eiCSV_sFileContents] = ''

EndFunc   ;==>_CSV_FileRead


Func _CSV_FileWrite(ByRef $hCSV)

	Local $vRet = FileWrite($hCSV[$eiCSV_hFile], $hCSV[$eiCSV_sFileContents])

	If Not $vRet Then Return SetError(1)

	_CSV_SetStatus($hCSV, $eiCSV_bModified)

EndFunc   ;==>_CSV_FileWrite


Func _CSV_FileClose(ByRef Const $hCSV)

	FileClose($hCSV[$eiCSV_hFile])

EndFunc   ;==>_CSV_FileClose


Func _CSV_FileSuits(ByRef Const $hCSV, ByRef Const $hGUI)

	Return StringInStr($hCSV[$eiCSV_sFileContents], $hGUI[$eiGUI_sSearch]) ? True : False

EndFunc   ;==>_CSV_FileSuits


Func _CSV_FileChangeContents(ByRef $hCSV, ByRef Const $hGUI)

	$hCSV[$eiCSV_sFileContents] = StringReplace($hCSV[$eiCSV_sFileContents], _
			$hGUI[$eiGUI_sSearch], $hGUI[$eiGUI_sReplace])

EndFunc   ;==>_CSV_FileChangeContents

#EndRegion ; класс CSV


#Region ; выполнение

GUI_Init()

While 1 ; простой GUI

	Sleep(100)
WEnd


Func ExecOperations()

	Local $hGUI = GUI_Data(@GUI_WinHandle)

	GUIDelete(@GUI_WinHandle)

	Local $hMessage = Message()

	Local $hCSV = CSV()

	CSV_SetFiles($hCSV, $hMessage)

	If @error Then Message_ShowSavedExit($hMessage)

	CSV_ProcessFiles($hCSV, $hGUI)

	CSV_CheckFlags($hCSV, $hMessage)

	If @error Then Message_ShowSavedExit($hMessage)

	MsgBox($MB_ICONINFORMATION, $esMsgBoxTitle, "Работа завершена")

	Exit

EndFunc   ;==>ExecOperations

#EndRegion ; выполнение
